import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { job } from './job.entity';
import { purchase } from './purchase.entity';

@Entity()
export class invoice {
  @PrimaryColumn()
  invoiceId: number;

  @Column()
  date: string;

  @Column()
  vat: number;

  @Column()
  discount: number;

  @Column()
  for: string;

  @ManyToOne(() => job, (job) => job.invoices)
  job: job;

  @OneToMany(() => purchase, (purchase) => purchase.invoice)
  purchases: purchase[];
}
