import {
  Column,
  Entity,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { job } from './job.entity';

@Entity()
export class permisionLetter {
  @PrimaryGeneratedColumn()
  permisionLetterId: number;

  @Column()
  letterNo: string;

  @Column()
  date: string;

  @Column()
  budgetCode: string;

  @Column()
  activityCode: string;

  @OneToMany(() => job, (job) => job.permisionLetter)
  jobs: job[];
}
