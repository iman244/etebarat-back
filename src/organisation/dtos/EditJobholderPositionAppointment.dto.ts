import { IsDate, IsNotEmpty, Matches } from 'class-validator';

export class EditJobholderPositionAppointmentDto {
  @IsNotEmpty({ message: 'نام موقعیت افسر خرید الزامی است' })
  positionName: string;

  personnelId: number;
}
