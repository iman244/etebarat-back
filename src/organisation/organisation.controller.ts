import { Body, Controller, Delete, Get, Patch, Post } from '@nestjs/common';
import { OrganisationService } from './organisation.service';
import { CreatePersonnelDto } from './dtos/CreatePersonnel.dto';
import { CreatePositionDto } from './dtos/CreatePosition.dto';
import { EditPositionDto } from './dtos/EditPosition.dto';
import { CreateJobholderPositionDto } from './dtos/CreateJobholderPosition.dto';
import { EditJobholderPositionAppointmentDto } from './dtos/EditJobholderPositionAppointment.dto';

@Controller('organisation')
export class OrganisationController {
  constructor(private readonly OrganisationService: OrganisationService) {}

  @Post('personnel')
  async newPersonnel(@Body() CreatePersonnelDto: CreatePersonnelDto) {
    return await this.OrganisationService.newPersonnel(CreatePersonnelDto);
  }

  @Patch('personnel')
  async editPersonnel(@Body() CreatePersonnelDto: CreatePersonnelDto) {
    return await this.OrganisationService.editPersonnel(CreatePersonnelDto);
  }

  @Delete('personnel')
  async deletePersonnel(@Body() CreatePersonnelDto: CreatePersonnelDto) {
    return await this.OrganisationService.deletePersonnel(CreatePersonnelDto);
  }

  @Get('personnel')
  async allPersonnel() {
    return await this.OrganisationService.allPersonnel();
  }

  @Post('position')
  async newPosition(@Body() CreatePositionDto: CreatePositionDto) {
    return await this.OrganisationService.newPosition(CreatePositionDto);
  }

  @Patch('position')
  async editPositon(@Body() EditPositionDto: EditPositionDto) {
    console.log('EditPositionDto', EditPositionDto);
    return await this.OrganisationService.editPosition(EditPositionDto);
  }

  @Delete('position')
  async deletePosition(@Body() CreatePositionDto: CreatePositionDto) {
    return await this.OrganisationService.deletePosition(CreatePositionDto);
  }

  @Get('position')
  async allPositions() {
    return await this.OrganisationService.allPositions();
  }

  @Post('jobholder')
  async newJobholder(
    @Body() CreateJobholderPositionDto: CreateJobholderPositionDto,
  ) {
    return await this.OrganisationService.newJobholder(
      CreateJobholderPositionDto,
    );
  }

  @Patch('jobholder/appointment')
  async editJobholder(
    @Body()
    EditJobholderPositionAppointmentDto: EditJobholderPositionAppointmentDto,
  ) {
    return await this.OrganisationService.editJobholderAppointment(
      EditJobholderPositionAppointmentDto,
    );
  }

  @Delete('jobholder')
  async deleteJobholder(
    @Body() CreateJobholderPositionDto: CreateJobholderPositionDto,
  ) {
    return await this.OrganisationService.deleteJobholder(
      CreateJobholderPositionDto,
    );
  }

  @Get('jobholder')
  async allJobholders() {
    return await this.OrganisationService.allJobholders();
  }
}
