import { IsNotEmpty, Matches } from 'class-validator';

export class issueInvoiceNumberDto {
  @IsNotEmpty({ message: `شماره فاکتور خرید الزامی است` })
  @Matches(/^[0-9]{4}$/, {
    message: 'شماره فاکتور باید 4 رقم باشد',
  })
  invoiceId: number;
}
