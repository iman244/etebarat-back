import { IsNotEmpty } from 'class-validator';

export class purchaseDto {
  @IsNotEmpty({ message: `شماره فاکتور خرید الزامی است` })
  invoiceId: number;

  @IsNotEmpty({ message: `شرح کالا الزامی است` })
  description: string;

  @IsNotEmpty({ message: `واحد خرید الزامی است` })
  unit: string;

  @IsNotEmpty({ message: `حجم خرید الزامی است` })
  quantity: number;

  @IsNotEmpty({ message: `مبلغ واحد الزامی است` })
  unitPrice: number;

  @IsNotEmpty({ message: `مبلغ خرید الزامی است` })
  purchaseValue: number;
}
