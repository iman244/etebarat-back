import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { jobholder } from 'src/entities/jobholder.entity';
import { personnel } from 'src/entities/personnel.entity';
import { position } from 'src/entities/position.entity';
import { Repository } from 'typeorm';
import { CreatePersonnelDto } from './dtos/CreatePersonnel.dto';
import { CreatePositionDto } from './dtos/CreatePosition.dto';
import { EditPositionDto } from './dtos/EditPosition.dto';
import { CreateJobholderPositionDto } from './dtos/CreateJobholderPosition.dto';
import { EditJobholderPositionAppointmentDto } from './dtos/EditJobholderPositionAppointment.dto';

@Injectable()
export class OrganisationService {
  constructor(
    @InjectRepository(personnel)
    private personnelRepository: Repository<personnel>,
    @InjectRepository(position)
    private positionRepository: Repository<position>,
    @InjectRepository(jobholder)
    private jobholderRepository: Repository<jobholder>,
  ) {}

  async newPersonnel(CreatePersonnelDto: CreatePersonnelDto) {
    let person = await this.checkPerson({
      personnelId: CreatePersonnelDto.personnelId,
      foundError: true,
    });
    try {
      let newPerson = await this.personnelRepository.save(CreatePersonnelDto);
      return {
        message: `${newPerson.rank} ${newPerson.name} ${newPerson.lastName} با موفقیت ثبت شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async editPersonnel(CreatePersonnelDto: CreatePersonnelDto) {
    let person = await this.checkPerson({
      personnelId: CreatePersonnelDto.personnelId,
      notFoundError: true,
    });
    if (person) {
      person.name = CreatePersonnelDto.name;
      person.lastName = CreatePersonnelDto.lastName;
      person.rank = CreatePersonnelDto.rank;
      try {
        let updatedPerson = await this.personnelRepository.save(person);
        return {
          message: `${updatedPerson.rank} ${updatedPerson.name} ${updatedPerson.lastName} با موفقیت به روز شد`,
        };
      } catch (error) {
        return error;
      }
    }
  }

  async deletePersonnel(CreatePersonnelDto: CreatePersonnelDto) {
    let person = await this.checkPerson({
      personnelId: CreatePersonnelDto.personnelId,
      notFoundError: true,
    });

    console.log('person', person);

    if (person.position?.positionName || person.jobholder?.positionName) {
      throw new HttpException(
        `${person.rank} ${person.name} ${person.lastName} در سازمان دارای مسئولیت است \n ابتدا فرد را از مسئولیت‌ها رها کنید و دوباره تلاش کنید`,
        HttpStatus.NOT_FOUND,
      );
    }

    const targetPerson = `${person.rank} ${person.name} ${person.lastName}`;

    try {
      let deletedPerson = await this.personnelRepository.remove(person);
      return {
        message: `${targetPerson} با موفقیت حذف شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async allPersonnel() {
    return await this.personnelRepository.find({
      relations: ['position', 'jobholder'],
    });
  }

  async checkPerson({
    personnelId,
    foundError = false,
    notFoundError = false,
  }: {
    personnelId: number;
    foundError?: boolean;
    notFoundError?: boolean;
  }) {
    let [person] = await this.personnelRepository.find({
      where: { personnelId },
      relations: ['position', 'jobholder'],
    });
    if (!person && notFoundError) {
      throw new HttpException(
        `فردی در سازمان با کد پرسنلی ${personnelId} پیدا نشد`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (person && foundError) {
      throw new HttpException(
        `فردی در سازمان با کد پرسنلی ${personnelId} قبلاً ایجاد شده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    return person;
  }

  async newPosition(CreatePositionDto: CreatePositionDto) {
    let position = await this.positionRepository.findOneBy({
      positionName: CreatePositionDto.positionName,
    });
    if (position)
      throw new HttpException(
        `موقعیت ${CreatePositionDto.positionName} قبلاً تعریف شده است`,
        HttpStatus.NOT_FOUND,
      );
    try {
      let newPosition = await this.positionRepository.save(CreatePositionDto);
      return {
        message: `موقعیت ${newPosition.positionName} با موفقیت اضافه شد`,
      };
    } catch (error) {
      return error;
    }
  }

  // we dont want edit position for changing name of position, in that case simply delete and create new position,
  // this case is for changing authorities.
  async editPosition(EditPositionDto: EditPositionDto) {
    let position = await this.checkPosition({
      positionName: EditPositionDto.positionName,
      notFoundError: true,
    });
    let person = await this.checkPerson({
      personnelId: EditPositionDto.personnelId,
      notFoundError: true,
    });

    // removing person from a position
    if (!EditPositionDto.personnelId) {
      position.person = null;
      try {
        let updatedPosition = await this.positionRepository.save(position);
        return {
          message: `موقعیت ${updatedPosition.positionName} خالی شد`,
        };
      } catch (error) {
        return error;
      }
    }

    // if person have previous position it must first remove from previos one and then add in new one
    if (
      person.position &&
      person.position.positionName === position.positionName
    ) {
      return await this.positionRepository.save(position);
    } else if (
      person.position &&
      person.position.positionName !== position.positionName
    ) {
      let [prevPosition] = await this.positionRepository.find({
        where: { positionName: person.position.positionName },
        relations: ['person'],
      });
      throw new HttpException(
        `این فرد موقعیت ${prevPosition.positionName} را در اختیار دارد \n ابتدا فرد را از موقعیت قبل حذف کنید و دوباره تلاش کنید`,
        HttpStatus.NOT_FOUND,
      );
    }

    // if position slot was empty and person doesn't have any position
    position.person = person;
    try {
      let updatedPosition = await this.positionRepository.save(position);
      return {
        message: `${person.rank} ${person.name} ${person.lastName} به موقعیت ${position.positionName} منصوب شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async deletePosition(CreatePositionDto: CreatePositionDto) {
    let position = await this.positionRepository.findOneBy({
      positionName: CreatePositionDto.positionName,
    });
    // const {personnelId, ...otherInformation} = CreatePersonnelDto
    const targetPositionName = position.positionName;
    if (position) {
      try {
        let deletedPosition = await this.positionRepository.remove(position);
        return {
          message: `موقعیت ${targetPositionName} حذف شد`,
        };
      } catch (error) {
        return error;
      }
    }
    throw new HttpException(
      `موقعیت ${CreatePositionDto.positionName} تعریف نشده است`,
      HttpStatus.NOT_FOUND,
    );
  }

  async allPositions() {
    return await this.positionRepository.find({ relations: ['person'] });
  }

  async checkPosition({
    positionName,
    foundError = false,
    notFoundError = false,
  }: {
    positionName: string;
    foundError?: boolean;
    notFoundError?: boolean;
  }) {
    let [position] = await this.positionRepository.find({
      where: { positionName },
      relations: ['person'],
    });
    if (!position && notFoundError) {
      throw new HttpException(
        `موقعیت ${positionName} تعریف نشده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (position && foundError) {
      throw new HttpException(
        `افسر خرید ${positionName} قبلاً ایجاد شده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    return position;
  }

  async newJobholder(CreateJobholderPositionDto: CreateJobholderPositionDto) {
    try {
      let newJobholder = await this.jobholderRepository.save(
        CreateJobholderPositionDto,
      );
      return {
        message: `موقعیت ${newJobholder.positionName} ایجاد شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async editJobholderAppointment(
    EditJobholderPositionAppointmentDto: EditJobholderPositionAppointmentDto,
  ) {
    let jobholder = await this.checkJobholder({
      positionName: EditJobholderPositionAppointmentDto.positionName,
      notFoundError: true,
    });
    let person = await this.checkPerson({
      personnelId: EditJobholderPositionAppointmentDto.personnelId,
      notFoundError: true,
    });

    if (!EditJobholderPositionAppointmentDto.personnelId) {
      jobholder.person = null;
      try {
        let updatedPosition = await this.jobholderRepository.save(jobholder);
        return {
          message: `موقعیت افسر خرید ${updatedPosition.positionName} خالی شد`,
        };
      } catch (error) {
        return error;
      }
    }
    if (
      person.jobholder &&
      person.jobholder.positionName === jobholder.positionName
    ) {
      return await this.jobholderRepository.save(jobholder);
    } else if (
      person.jobholder &&
      person.jobholder.positionName !== jobholder.positionName
    ) {
      let [prevJobholder] = await this.jobholderRepository.find({
        where: { positionName: person.jobholder.positionName },
        relations: ['person'],
      });
      throw new HttpException(
        `این فرد موقعیت افسر خرید ${prevJobholder.positionName} را در اختیار دارد \n ابتدا فرد را از موقعیت افسر خرید قبل حذف کنید و دوباره تلاش کنید`,
        HttpStatus.NOT_FOUND,
      );
    }

    // if position slot was empty and person doesn't have any position
    jobholder.person = person;
    try {
      let updatedJobholder = await this.jobholderRepository.save(jobholder);
      return {
        message: `${person.rank} ${person.name} ${person.lastName} به موقعیت افسر خرید ${jobholder.positionName} منصوب شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async deleteJobholder(
    CreateJobholderPositionDto: CreateJobholderPositionDto,
  ) {
    let jobholder = await this.checkJobholder({
      positionName: CreateJobholderPositionDto.positionName,
      notFoundError: true,
    });

    const targetJobholderPositionName = jobholder.positionName;

    try {
      let deletedJobholder = await this.jobholderRepository.remove(jobholder);
      return {
        message: `موقعیت افسر خرید ${targetJobholderPositionName} حذف شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async allJobholders() {
    return await this.jobholderRepository.find({ relations: ['person'] });
  }

  async checkJobholder({
    positionName,
    foundError = false,
    notFoundError = false,
  }: {
    positionName: string;
    foundError?: boolean;
    notFoundError?: boolean;
  }) {
    let [jobholder] = await this.jobholderRepository.find({
      where: { positionName },
      relations: ['person'],
    });
    if (!jobholder && notFoundError) {
      throw new HttpException(
        `موقعیت افسر خرید ${positionName} تعریف نشده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (jobholder && foundError) {
      throw new HttpException(
        `افسر خرید ${positionName} قبلاً ایجاد شده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    return jobholder;
  }
}
