import { IsDate, IsNotEmpty, Matches } from 'class-validator';

export class PositionAppointmentDto {
  @IsNotEmpty({ message: 'نام موقعیت الزامی است' })
  positionName: string;

  @IsNotEmpty({ message: 'شماره پرسنلی فرد معرفی شده الزامی است' })
  personnelId: string;
}
