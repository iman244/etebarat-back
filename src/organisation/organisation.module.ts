import { Module } from '@nestjs/common';
import { OrganisationController } from './organisation.controller';
import { OrganisationService } from './organisation.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { personnel } from 'src/entities/personnel.entity';
import { position } from 'src/entities/position.entity';
import { jobholder } from 'src/entities/jobholder.entity';

@Module({
  imports: [TypeOrmModule.forFeature([personnel, position, jobholder])],
  controllers: [OrganisationController],
  providers: [OrganisationService],
  exports: [OrganisationService],
})
export class OrganisationModule {}
