import { MigrationInterface, QueryRunner } from 'typeorm';

export class Migrate1698336164052 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permision_letter" RENAME COLUMN "LetterNo" TO "letterNo"`,
    );
    // await queryRunner.query(
    //   `ALTER TABLE "permision_letter" RENAME COLUMN "BudgetCode" TO "budgetCode"`,
    // );
    // await queryRunner.query(
    //   `ALTER TABLE "permision_letter" RENAME COLUMN "ActivityCode" TO "activityCode"`,
    // );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "permision_letter" RENAME COLUMN "letterNo" TO "LetterNo"`,
    );
    // await queryRunner.query(
    //   `ALTER TABLE "permision_letter" RENAME COLUMN "budgetCode" TO "BudgetCode"`,
    // );
    // await queryRunner.query(
    //   `ALTER TABLE "permision_letter" RENAME COLUMN "activityCode" TO "ActivityCode"`,
    // );
  }
}
