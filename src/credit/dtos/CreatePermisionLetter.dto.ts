import { IsDate, IsNotEmpty, Matches } from 'class-validator';

export class CreatePermisionLetterDto {
  @IsNotEmpty({ message: 'ورود شماره نامه الزامی است' })
  letterNo: string;

  @IsNotEmpty({ message: 'ورود تاریخ الزامی است' })
  @Matches(/14[0-9]{2}\/(0|1)[0-9]\/([0-3][0-9])$/g, {
    message: 'تاریخ را با فرمت ##/##/##14 ارسال کنید',
  })
  date: string;

  @IsNotEmpty({ message: 'ورود کد بودجه الزامی است' })
  budgetCode: string;

  @IsNotEmpty({ message: 'ورود شماره فعالیت الزامی است' })
  activityCode: string;
}
