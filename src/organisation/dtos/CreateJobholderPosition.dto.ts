import { IsDate, IsNotEmpty, Matches } from 'class-validator';

export class CreateJobholderPositionDto {
  @IsNotEmpty({ message: 'نام موقعیت افسر خرید الزامی است (مثال: برنامه 1)' })
  positionName: string;
}
