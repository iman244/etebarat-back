import { IsDate, IsNotEmpty, Matches } from 'class-validator';

export class EditPersonnelDto {
  @IsNotEmpty({ message: 'شماره پرسنلی فرد معرفی شده الزامی است' })
  personnelId: number;

  @IsNotEmpty({ message: 'نام الزامی است' })
  name: string;

  @IsNotEmpty({ message: 'نام خانوادگی الزامی است' })
  lastName: string;

  @IsNotEmpty({ message: 'درجه فرد الزامی است' })
  rank: string;
}
