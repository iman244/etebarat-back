import { Column, Entity, OneToMany, OneToOne, PrimaryColumn } from 'typeorm';
import { job } from './job.entity';
import { position } from './position.entity';
import { jobholder } from './jobholder.entity';

@Entity()
export class personnel {
  @PrimaryColumn()
  personnelId: number;

  @Column()
  name: string;

  @Column()
  lastName: string;

  @Column()
  rank: string;

  @OneToOne(() => position, (position) => position.person)
  position: position;

  @OneToOne(() => jobholder, (jobholder) => jobholder.person)
  jobholder: jobholder;

  @OneToMany(() => job, (job) => job.personnel)
  jobs: job[];
}
