import { IsDate, IsNotEmpty, Matches } from 'class-validator';

export class CreatePositionDto {
  @IsNotEmpty({ message: 'نام موقعیت الزامی است' })
  positionName: string;
}
