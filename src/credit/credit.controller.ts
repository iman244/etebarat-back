import { Controller, Get, Post, Body, Delete } from '@nestjs/common';
import { CreatePermisionLetterDto } from './dtos/CreatePermisionLetter.dto';
import { CreditService } from './credit.service';
import { CreateJobDto } from './dtos/CreateJob.dto';
import { DeleteJobDto } from './dtos/DeleteJob.dto';
import { purchaseDto } from './dtos/Purhcase.dto';
import { issueInvoiceDto } from './dtos/issueInvoice.dto';
import { issueInvoiceNumberDto } from './dtos/issueInvoiceNumber.dto';
import { DeleteInvoiceDto } from './dtos/DeleteInvoice.dto';

@Controller('credit')
export class CreditController {
  constructor(private readonly CreditService: CreditService) {}

  @Get('permision-letters')
  async getAllPermisions() {
    return await this.CreditService.getPermisionsLetter();
  }

  @Post('permision-letter')
  async createPermisionLetter(
    @Body() CreatePermisionLetterDto: CreatePermisionLetterDto,
  ) {
    return await this.CreditService.newPermisionLetter(
      CreatePermisionLetterDto,
    );
  }

  @Post('job')
  async createJob(@Body() CreateJobDto: CreateJobDto) {
    return await this.CreditService.newJob(CreateJobDto);
  }

  @Get('job')
  async getJobs() {
    return await this.CreditService.getJobs();
  }

  @Delete('job')
  async DeleteJob(@Body() DeleteJobDto: DeleteJobDto) {
    return await this.CreditService.deleteJob(DeleteJobDto);
  }

  @Post('purchase')
  async purchase(@Body() purchaseDto: purchaseDto) {
    return await this.CreditService.purchase(purchaseDto);
  }

  @Get('invoice')
  async getInvoices() {
    return await this.CreditService.getInvoices();
  }

  @Post('invoice')
  async issueInvoiceNumber(
    @Body() issueInvoiceNumberDto: issueInvoiceNumberDto,
  ) {
    return await this.CreditService.issueInvoiceNumber(issueInvoiceNumberDto);
  }

  @Delete('invoice')
  async DeleteInvoice(@Body() DeleteInvoiceDto: DeleteInvoiceDto) {
    return await this.CreditService.deleteInvoice(DeleteInvoiceDto);
  }
}
