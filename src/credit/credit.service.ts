import { HttpException, HttpStatus, Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { permisionLetter } from 'src/entities/permisionLetter.entity';
import { Repository } from 'typeorm';
import { CreatePermisionLetterDto } from './dtos/CreatePermisionLetter.dto';
import { CreateJobDto } from './dtos/CreateJob.dto';
// import { CreateJobholderDto } from '../personnel/dtos/CreatePersonnel.dto';
import { jobholder } from 'src/entities/jobholder.entity';
import { OrganisationService } from 'src/organisation/organisation.service';
import { job } from 'src/entities/job.entity';
import { DeleteJobDto } from './dtos/DeleteJob.dto';
import { purchaseDto } from './dtos/Purhcase.dto';
import { invoice } from 'src/entities/invoice.entity';
import { purchase } from 'src/entities/purchase.entity';
import { issueInvoiceDto } from './dtos/issueInvoice.dto';
import { issueInvoiceNumberDto } from './dtos/issueInvoiceNumber.dto';
import { DeleteInvoiceDto } from './dtos/DeleteInvoice.dto';

@Injectable()
export class CreditService {
  constructor(
    @Inject(OrganisationService)
    private readonly OrganisationService: OrganisationService,

    @InjectRepository(permisionLetter)
    private permisionLetterRepository: Repository<permisionLetter>,

    @InjectRepository(job)
    private jobRepository: Repository<job>,

    @InjectRepository(jobholder)
    private jobholderRepository: Repository<jobholder>,

    @InjectRepository(invoice)
    private invoiceRepository: Repository<invoice>,

    @InjectRepository(purchase)
    private purchaseRepository: Repository<purchase>,
  ) {}

  async newPermisionLetter(CreatePermisionLetterDto: CreatePermisionLetterDto) {
    let find = await this.permisionLetterRepository.findOneBy({
      letterNo: CreatePermisionLetterDto.letterNo,
    });
    if (find)
      throw new HttpException(
        `نامه ${CreatePermisionLetterDto.letterNo} قبلاً ایجاد شده است`,
        HttpStatus.CONFLICT,
      );
    return await this.permisionLetterRepository.save(CreatePermisionLetterDto);
  }

  async getPermisionsLetter() {
    return await this.permisionLetterRepository.find({
      relations: ['jobs', 'jobs.personnel.jobholder'],
    });
  }

  async checkPermisionLetter({
    permisionLetterId,
    foundError = false,
    notFoundError = false,
  }: {
    permisionLetterId: number;
    foundError?: boolean;
    notFoundError?: boolean;
  }) {
    let [permisionLetter] = await this.permisionLetterRepository.find({
      where: { permisionLetterId },
      relations: ['jobs'],
    });
    if (!permisionLetter && notFoundError) {
      throw new HttpException(
        `نامه با شناسه ${permisionLetterId} پیدا نشد`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (permisionLetter && foundError) {
      throw new HttpException(
        `نامه ${permisionLetter.letterNo} - ${permisionLetter.date} قبلاً ایجاد شده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    return permisionLetter;
  }

  async newJob(CreateJobDto: CreateJobDto) {
    let { permisionLetterId, personnelId, ...someFieldsOfJob } = CreateJobDto;
    let permisionLetter = await this.checkPermisionLetter({
      permisionLetterId,
      notFoundError: true,
    });
    let person = await this.OrganisationService.checkPerson({
      personnelId,
      notFoundError: true,
    });

    let job = {
      ...someFieldsOfJob,
      personnel: person,
    };

    try {
      let newJob = await this.jobRepository.save(job);
      permisionLetter.jobs.push(newJob);
      let updatedPermisionLetter =
        this.permisionLetterRepository.save(permisionLetter);
      return {
        message: `مبلغ ${newJob.creditValue} به اعتبار افسر خرید ${person.jobholder.positionName} اضافه شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async getJobs() {
    return await this.jobRepository.find({
      relations: ['permisionLetter', 'personnel', 'invoices'],
    });
  }

  async deleteJob(DeleteJobDto: DeleteJobDto) {
    let job = await this.checkJob({
      jobId: DeleteJobDto.jobId,
      notFoundError: true,
    });
    const targetJobCreditValue = job.creditValue;
    try {
      let deletedJob = await this.jobRepository.remove(job);
      return {
        message: `اعتبار به مبلغ ${targetJobCreditValue} حذف شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async checkJob({
    jobId,
    foundError = false,
    notFoundError = false,
  }: {
    jobId: number;
    foundError?: boolean;
    notFoundError?: boolean;
  }) {
    let [job] = await this.jobRepository.find({
      where: { jobId },
      relations: ['permisionLetter', 'personnel', 'invoices'],
    });
    if (!job && notFoundError) {
      throw new HttpException(
        `اعتبار با شناسه ${jobId} پیدا نشد`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (job && foundError) {
      throw new HttpException(
        `اعتبار با شناسه ${jobId} قبلاً ایجاد شده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    return job;
  }

  async purchase(purchaseDto: purchaseDto) {
    let { invoiceId, ...purchaseFields } = purchaseDto;
    let invoice = await this.checkInvoice({
      invoiceId,
      makeOnNotFound: true,
    });
    try {
      let purchase = await this.purchaseRepository.save(purchaseFields);
      invoice.purchases.push(purchase);
      return {
        message: `فاکتور ${invoiceId} به روز شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async checkInvoice({
    invoiceId,
    makeOnNotFound = false,
    foundError = false,
    notFoundError = false,
  }: {
    invoiceId: number;
    makeOnNotFound?: boolean;
    foundError?: boolean;
    notFoundError?: boolean;
  }) {
    let [invoice] = await this.invoiceRepository.find({
      where: { invoiceId },
      relations: ['job', 'purchases'],
    });
    if (!invoice && notFoundError) {
      throw new HttpException(
        `اعتبار با شناسه ${invoiceId} پیدا نشد`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (invoice && foundError) {
      throw new HttpException(
        `اعتبار با شناسه ${invoiceId} قبلاً ایجاد شده است`,
        HttpStatus.NOT_FOUND,
      );
    }
    if (makeOnNotFound && !invoice && !notFoundError) {
      console.log('makeOnNotFound');
      let newInvoice = await this.invoiceRepository.save({
        invoiceId,
        date: '',
        discount: 0,
        vat: 0,
        for: '',
      });

      return newInvoice;
    }
    return invoice;
  }

  async getInvoices() {
    return await this.invoiceRepository.find({
      relations: ['job', 'purchases'],
    });
  }

  async deleteInvoice(DeleteInvoiceDto: DeleteInvoiceDto) {
    let invoice = await this.checkInvoice({
      invoiceId: DeleteInvoiceDto.invoiceId,
      notFoundError: true,
    });
    var targetInvoiceId = invoice.invoiceId;
    try {
      let deletedInvoice = await this.invoiceRepository.remove(invoice);
      return {
        message: `فاکتور ${targetInvoiceId} حذف شد`,
      };
    } catch (error) {
      return error;
    }
  }

  async issueInvoiceNumber(issueInvoiceNumberDto: issueInvoiceNumberDto) {
    let invoice = await this.checkInvoice({
      invoiceId: issueInvoiceNumberDto.invoiceId,
      makeOnNotFound: true,
    });

    return {
      message: `فاکتور ${invoice.invoiceId} ایجاد شد`,
    };
  }
}
