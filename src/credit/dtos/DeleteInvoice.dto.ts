import { IsNotEmpty, IsNumber, Matches } from 'class-validator';

export class DeleteInvoiceDto {
  @IsNotEmpty({ message: 'شماره فاکتور الزامی است' })
  @IsNumber(
    { allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 },
    { message: 'شماره فاکتور باید عدد باشد' },
  )
  // @Matches(/^[0-9]{4}$/, {
  //   message: 'شماره فاکتور باید 4 رقم باشد',
  // })
  invoiceId: number;
}
