import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';
import { job } from './job.entity';
import { personnel } from './personnel.entity';

@Entity()
export class position {
  @PrimaryColumn()
  positionName: string;

  @OneToOne(() => personnel, (personnel) => personnel.position)
  @JoinColumn()
  person: personnel;
}
