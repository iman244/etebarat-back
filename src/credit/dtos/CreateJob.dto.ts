import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateJobDto {
  @IsNotEmpty({ message: 'ورود شماره نامه الزامی است' })
  @IsNumber()
  permisionLetterId: number;

  @IsNotEmpty()
  @IsNumber({}, { message: 'شماره پرسنلی باید عدد باشد' })
  personnelId: number;

  @IsNotEmpty()
  @IsNumber({}, { message: 'کد ملاحظات باید عدد باشد' })
  molahezatCode: number;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  @IsNumber({}, { message: 'مبلغ اعتبار باید عدد باشد' })
  creditValue: number;
}
