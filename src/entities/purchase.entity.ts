import {
  Column,
  Entity,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { invoice } from './invoice.entity';

@Entity()
export class purchase {
  @PrimaryGeneratedColumn()
  purchaseId: string;

  @ManyToOne(() => invoice, (invoice) => invoice.purchases)
  invoice: invoice;

  @Column()
  description: string;

  @Column()
  unit: string;

  @Column()
  quantity: number;

  @Column()
  unitPrice: number;

  @Column()
  purchaseValue: number;
}
