import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { permisionLetter } from './permisionLetter.entity';
import { jobholder } from './jobholder.entity';
import { invoice } from './invoice.entity';
import { personnel } from './personnel.entity';

@Entity()
export class job {
  @PrimaryGeneratedColumn()
  jobId: number;

  @Column()
  molahezatCode: number;

  @Column()
  description: string;

  @Column()
  creditValue: number;

  @ManyToOne(() => permisionLetter, (permisionLetter) => permisionLetter.jobs)
  permisionLetter: permisionLetter;

  @ManyToOne(() => personnel, (personnel) => personnel.jobs)
  personnel: personnel;

  @OneToMany(() => invoice, (invoice) => invoice.job)
  invoices: invoice[];
}
