import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { job } from 'src/entities/job.entity';
import { jobholder } from 'src/entities/jobholder.entity';
import { permisionLetter } from 'src/entities/permisionLetter.entity';
import { CreditService } from './credit.service';
import { CreditController } from './credit.controller';
import { OrganisationService } from 'src/organisation/organisation.service';
import { personnel } from 'src/entities/personnel.entity';
import { position } from 'src/entities/position.entity';
import { OrganisationModule } from 'src/organisation/organisation.module';
import { purchase } from 'src/entities/purchase.entity';
import { invoice } from 'src/entities/invoice.entity';

@Module({
  imports: [
    OrganisationModule,
    TypeOrmModule.forFeature([
      permisionLetter,
      job,
      jobholder,
      personnel,
      position,
      invoice,
      purchase
    ]),
  ],
  providers: [CreditService, OrganisationService],
  controllers: [CreditController],
})
export class CreditModule {}
