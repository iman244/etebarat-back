import {
  Module,
  NestModule,
  MiddlewareConsumer,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreditController } from './credit/credit.controller';
import { CreditService } from './credit/credit.service';
import { LoggerMiddleware } from './logger.middleware ';
import { permisionLetter } from './entities/permisionLetter.entity';
import { PurchaseController } from './purchase/purchase.controller';
import { job } from './entities/job.entity';
import { jobholder } from './entities/jobholder.entity';
import { purchase } from './entities/purchase.entity';
import { invoice } from './entities/invoice.entity';
import { CreditModule } from './credit/credit.module';
import { OrganisationModule } from './organisation/organisation.module';
import { personnel } from './entities/personnel.entity';
import { position } from './entities/position.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mssql',
      host: 'localhost',
      port: 1433,
      // url: ';Server=localhost\\SQLEXPRESS;Database=master;Trusted_Connection=True;',
      username: 'iman244',
      password: '1234',
      database: 'Etebarat',
      entities: [
        personnel,
        position,
        permisionLetter,
        job,
        jobholder,
        purchase,
        invoice,
      ],
      synchronize: true,
      options: { encrypt: false },
      migrationsRun: true,
      migrations: ['../migrations/*{.ts}'],
    }),
    TypeOrmModule.forFeature([
      permisionLetter,
      job,
      jobholder,
      invoice,
      purchase,
    ]),
    CreditModule,
    OrganisationModule,
  ],
  controllers: [AppController, CreditController, PurchaseController],
  providers: [AppService, CreditService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
