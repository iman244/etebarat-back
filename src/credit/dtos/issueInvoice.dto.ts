import { IsNotEmpty, IsNumber, Matches } from 'class-validator';

export class issueInvoiceDto {
  @IsNotEmpty({ message: `شماره فاکتور خرید الزامی است` })
  @IsNumber(
    { allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 },
    { message: 'شماره فاکتور باید عدد باشد' },
  )
  @Matches(/^[0-9]{4}$/, {
    message: 'شماره فاکتور باید 4 رقم باشد',
  })
  invoiceId: number;

  @IsNotEmpty({ message: `محل اعتبار فاکتور الزامی است` })
  @IsNumber(
    { allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 },
    { message: 'شناسه اعتبار باید عدد باشد' },
  )
  jobId: number;

  @IsNotEmpty({ message: `تاریخ الزامی است` })
  @Matches(/14[0-9]{2}\/(0|1)[0-9]\/([0-3][0-9])$/g, {
    message: 'تاریخ را با فرمت ##/##/##14 ارسال کنید',
  })
  date: string;

  @IsNotEmpty({ message: `مبلغ مالیا بر ارزش افزوده الزامی است` })
  vat: number;

  @IsNotEmpty({ message: `مبلغ تخفیف الزامی است` })
  discount: number;

  @IsNotEmpty({ message: `مبلغ تخفیف الزامی است` })
  for: string;
}
