import { IsNotEmpty, IsNumber } from 'class-validator';

export class DeleteJobDto {
  @IsNotEmpty({ message: 'شناسه اعتبار الزامی است' })
  @IsNumber(
    { allowInfinity: false, allowNaN: false, maxDecimalPlaces: 0 },
    { message: 'شناسه اعتبار باید عدد باشد' },
  )
  jobId: number;
}
